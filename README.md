# Testovaci skript pro spc

Testovací skript určený pro studenty IZP 2020/21. Skript nahrajte do adresáře s
právem zápisu vedle souboru spc.c a spusťte.

```sh
$ git clone https://gitlab.com/Baerwin/izp-prj2-tests
$ cp sps.c spctest
$ cd spstest
$ chmod +x spstest.sh
$ ./spstest.sh
```

Disclaimer:
Toto jsou moje osobní opravy a absolutně za ně neručím

Všechna práva vyhrazena Doktoru Smrčkovi
https://pajda.fit.vutbr.cz/smrcka/spctest/
